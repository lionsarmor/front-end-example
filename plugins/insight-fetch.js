export default ({ store, app }, inject) => {
    inject('insightfetch', (extendUrl, fetchType='GET', jsonBody=null,  mode='cors', credential='same-origin', alert=false) => {
        let token = sessionStorage.getItem('token')
        let base = store.state.apiEndPoint
        let url = base + extendUrl
        let headers = {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(token + ':unused')
        }
        let args = { 
            credentials: credential,
            headers: headers,
            mode:mode,
            method: fetchType 
        }
        if (jsonBody != null){
            args['body'] = JSON.stringify(jsonBody)
        }
        let response = fetch(url, args).then(function(response) {
            if (response.status!==200) {
                app.router.push("/login")
              }
              if (alert==true) {
              if (typeof response.message != "undefined"){
                alert("Message"+response.message);
              } else {
                alert("Request Failed")}
            }
              return response.json()
            })
        return response
      })
    } 
