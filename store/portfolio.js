export const state = () => ({
    isLoading : true,
    options: {},
    series: []
  })

export const mutations = {
  loader(state, value){
    state.isLoading = value;
    },
  updateSeries(state, value){
    state.series = value
  },
  updateOptions(state, value){
    state.options = value
  }
}