export const state = () => ({
  //apiEndPoint: 'http://localhost:5000/',
  //apiEndPoint: 'http://0.0.0.0:5000/',
  apiEndPoint: 'https://api.insightx.info/',
  // Fetch Variables
  authUser: null,
  currentPage: null,
  tickerLoading: false,
  generalData:null,
  defaultFiat: null,
  superUser: null,
  retrieving: false,
  paidMember: null,
})

export const mutations = {
  SET_USER: function (state, user) {
    state.authUser = user
  },
  updateBalances (state, newBalances) {
    state.generalData.balances = newBalances
  },
  updateKey (state, kargs) {
    let key = kargs['key']
    state.generalData[key] = kargs['newData']
  },
  updatePage (state, page) {
    state.currentPage = page
  },
  updateRetrieving (state) {
    state.retrieving = true
  },
  updateTickerLoader(state){
    state.tickerLoading = !state.tickerLoading
  },
  updateValidToken(state){
    state.hasValidToken = true;
  },
  clearGeneralData(state){
    state.generalData = null
    state.defaultFiat = null
    state.superUser = null
    state.paymentStatus = null
    state.retrieving = false
    state.tickerLoading = false
  },
  updateGeneralData(state, data){
    state.generalData = data;
    state.defaultFiat = data.userOptions.fiat
    state.superUser = data.userInfo.superUser
    state.paidMember = data.userInfo.paidMember
  }
}
