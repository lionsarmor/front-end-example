export const state = () => ({
    authUser: null,
    tickerData: null,
    totalMarketCap: null,
  })

export const mutations = {
    SET_USER: function (state, user) {
        console.log(user)
        if (user == 'logout'){
            state.authUser = 'loggedOut'
        } else {    
            state.authUser = user
        }
        },
        updateTickerData(state, payload){
        state.tickerData = payload
        },
        updateTotalMarketCap(state, payload){
        state.totalMarketCap = payload
        },
        // GENERAL DATA 
        updateGeneral (state, page) {
            state.generalData = page
          },
    }